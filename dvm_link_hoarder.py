#!/usr/bin/env python

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

import time

def find_index(links, current_page):
  for link in links:
    string_needed = 'Page$%s' % str(current_page)
    string_link = str(link.get_attribute('href'))
    if string_needed in string_link:
      return links.index(link)

arr_of_links = []
num_of_pages = None
current_page = 6

driver = webdriver.Chrome('/Users/aether/Downloads/chromedriver')
driver.set_page_load_timeout(10)

driver.get("https://www.dvminsight.com/ImageLibrary/SearchPage.aspx")

driver.find_element_by_id('TextBox1').send_keys('canine filminterp')
driver.find_element_by_id('Button1').send_keys(Keys.RETURN)

num_of_pages = int(driver.find_elements_by_tag_name('em')[0].text[-2:])

# make sure to change range (start_index, finish_index*) NOTE: finish_index is exclusive

for x in range(5, 10):
  print current_page
  results_for_page = len(driver.find_elements_by_xpath('//input[@value="Select"]'))

  # inner loop for results per page
  for y in range(0, results_for_page):

    if current_page > 1:
      index = find_index(driver.find_elements_by_tag_name('a'), current_page)
      driver.find_elements_by_tag_name('a')[index].send_keys(Keys.RETURN)

    driver.set_page_load_timeout(10)

    try:
      driver.find_elements_by_xpath('//input[@value="Select"]')[y].send_keys(Keys.RETURN)
      arr_of_links.append(driver.find_element_by_id('URLbox').get_attribute('value'))

    except TimeoutException as ex:
      if driver.find_element_by_id('URLbox').is_displayed():
        arr_of_links.append(driver.find_element_by_id('URLbox').get_attribute('value'))
      else:
        arr_of_links.append('Timeout Error')
    
    driver.set_page_load_timeout(60)

    driver.get("https://www.dvminsight.com/ImageLibrary/SearchPage.aspx")

    driver.find_element_by_id('TextBox1').send_keys('canine radiograph')
    driver.find_element_by_id('Button1').send_keys(Keys.RETURN)

  current_page += 1

driver.close()


f = open('links.txt', 'wb')
#print arr_of_links
for link in arr_of_links:
  f.write(link)
  f.write('\n')

f.close()