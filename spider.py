import scrapy

class DVMSpider(scrapy.Spider):
  name='Michael Jackson'

  def __init__(self, filename=None):
    if filename:
      with open(filename, 'rb') as f:
        test = f.readlines()
        self.start_urls = map(str.rstrip, test)
        f.close()

  def parse(self, response):
    print response.xpath('//table[@id="DetailsView1"]//td/text()').extract()
    print response.xpath('//table[@id="DetailsView4"]//td/text()').extract()
