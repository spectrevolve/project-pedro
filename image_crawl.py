import scrapy
from scrapy import Request

class ImageSpider(scrapy.Spider):
  name='image spider'

  start_urls=['https://www.dvminsight.com/ImageLibrary/JPGViewer/imagemaker.aspx?DEXAM=21383&IMAGENAME=IMAGE_0001.jpg&FLIP=False&ROTATE=0']

  def parse(self, response):
    yield Request(self.start_urls[0], callback=self.save_image)

  def save_image(self, response):
    path = '/Users/aether/Documents/webscraper/t.jpg'
    with open(path, "wb") as f:
      f.write(response.body)